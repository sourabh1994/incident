package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPendingIssuesResponsePojo {
    @SerializedName("Message")
    @Expose
    private String Message;

    @SerializedName("Success")
    @Expose
    private String Success;

    @SerializedName("Data")
    @Expose
    private List<GetPendingIssuesResponseDataPojo> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public List<GetPendingIssuesResponseDataPojo> getData() {
        return Data;
    }

    public void setData(List<GetPendingIssuesResponseDataPojo> data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"Message\": \"" + Message + "\",\n" +
                "  \"Success\": \"" + Success + "\",\n" +
                "  \"Data\": " + Data + "\n" +
                "}";
    }

}
