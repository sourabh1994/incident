package googlemapsdemo.kmg.com.demo.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import googlemapsdemo.kmg.com.demo.Form;
import googlemapsdemo.kmg.com.demo.Pojo.GetPendingIssuesResponseDataPojo;
import googlemapsdemo.kmg.com.demo.Pojo.IssuePojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryPojo;
import googlemapsdemo.kmg.com.demo.R;
import googlemapsdemo.kmg.com.demo.utils.Util;

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.MyViewHolder> {
    private List<GetPendingIssuesResponseDataPojo> dataList;
    Context context;

    public IssueAdapter(@NonNull List<GetPendingIssuesResponseDataPojo> dataList, Context context) {
        this.dataList = dataList;
        this.context=context;
    }

    @Override
    public IssueAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.sub_category_content, viewGroup, false);

        return new IssueAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueAdapter.MyViewHolder myViewHolder, int i) {
        GetPendingIssuesResponseDataPojo data = dataList.get(i);
        myViewHolder.title.setText(data.getSubCategoryName());
        myViewHolder.subTitle.setText(data.getDescription());
//        Util.invalidatePicasso(data.getImages());
//        String url = data.getIcon();
//        Picasso.get().load(url).into(myViewHolder.image);
        Util.invalidatePicasso(data.getSubCategoryIcon());
        String url = data.getSubCategoryIcon();
        Picasso.get().load(url).into(myViewHolder.imageView);

    }
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,subTitle;
        public LinearLayout categoryCard;
        public ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            subTitle = (TextView) itemView.findViewById(R.id.sub_title);
            categoryCard = (LinearLayout) itemView.findViewById(R.id.CategoryCard);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
