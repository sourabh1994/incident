package googlemapsdemo.kmg.com.demo.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

import googlemapsdemo.kmg.com.demo.Pojo.CategoryResponseDataPojo;
import googlemapsdemo.kmg.com.demo.R;
import googlemapsdemo.kmg.com.demo.SubCatFragment;
import googlemapsdemo.kmg.com.demo.utils.Util;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private List<CategoryResponseDataPojo> dataList;
    Context context;

    public CategoryAdapter(@NonNull List<CategoryResponseDataPojo> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_content, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        CategoryResponseDataPojo data = dataList.get(i);
       myViewHolder.title.setText(data.getCategoryName());
       //myViewHolder.subTitle.setText(data.getSubtitle());
//        Resources res = context.getResources();
//        TypedArray images= res.obtainTypedArray(R.array.images);
//        Drawable drawable = images.getDrawable(i);
//        myViewHolder.image.setImageDrawable(drawable);
        Util.invalidatePicasso(data.getIcon());
        String url = data.getIcon();
        Picasso.get().load(url).into(myViewHolder.image);


        myViewHolder.categoryCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                Bundle bundle = new Bundle();
                SubCatFragment subCatFragment = new SubCatFragment();
                bundle.putString("whichCategory", myViewHolder.title.getText().toString());
                bundle.putLong("CategoryId", dataList.get(i).getCategoryID());
                subCatFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.container, subCatFragment, "SubCategory");
                fragmentTransaction.addToBackStack("SubCategory");
                fragmentTransaction.commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subTitle;
        public LinearLayout categoryCard;
        public ImageView image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            subTitle = (TextView) itemView.findViewById(R.id.sub_title);
            categoryCard = (LinearLayout) itemView.findViewById(R.id.CategoryCard);
            image = (ImageView) itemView.findViewById(R.id.image);

        }
    }
}
