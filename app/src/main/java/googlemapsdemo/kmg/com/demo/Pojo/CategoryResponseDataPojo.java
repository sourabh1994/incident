package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryResponseDataPojo {

    @SerializedName("CategoryID")
    @Expose
    private long CategoryID;

    @SerializedName("CategoryName")
    @Expose
    private String CategoryName;

    @SerializedName("Icon")
    @Expose
    private String Icon;

    public long getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(long categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"CategoryID\": \" + CategoryID + \",\n" +
                "  \"CategoryName\": \"" + CategoryName + "\",\n" +
                "  \"Icon\": \"" + Icon + "\"\n" +
                "}";
    }

}
