package googlemapsdemo.kmg.com.demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;
import java.util.Spliterator;

import googlemapsdemo.kmg.com.demo.utils.Util;

public class Splash extends AppCompatActivity {
    private final int PERMISSION_LOCATION_REQUEST_CODE = 100;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 100;
    public int isNeverAskIf = 0, isNeverAskElse = 0, isNormalIf = 0, isNormalElse = 0;
    SharedPreferences sharedPrefUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPrefUser=getSharedPreferences(Util.sharedPrefUser, MODE_PRIVATE);
        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_LOCATION_REQUEST_CODE);

        } else {
            new Handler().postDelayed(() -> {
                if (sharedPrefUser.getString(Util.usreLoginStatus,"false").equalsIgnoreCase("true")){
                    Intent intent =new Intent(Splash.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }else
                {
                    Intent intent = new Intent(this, LoginActivity.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

            }, 500);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    isNeverAskIf = 0;
                    isNeverAskElse = 0;
                    isNormalIf = 0;
                    isNormalElse = 0;
                    for (int i = 0, len = permissions.length; i < len; i++) {
                        String permission = permissions[i];

                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (!showRationale) {
                                isNeverAskIf += 1;
                            } else {
                                isNeverAskElse += 1;
                            }
                        } else {
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                                isNormalIf = 5;
                            } else {
                                isNormalElse += 1;
                            }
                        }
                    }
                    if (isNormalIf == 5) {
                        Util.enableMyLocationIfPermitted(this);
                    } else if (isNeverAskElse != 0) {
                        Util.showPermissionAlert(false,this);
                    } else if (isNeverAskIf != 0 || isNormalElse != 0) {
                        Util.showPermissionAlert(true,this);
                    }
                } else {
                    Util.showPermissionAlert(false,this);
                }

            }
            break;
//                    else {
//                        showDefaultLocation();
//                    }
//                    return;
        }

    }

}
