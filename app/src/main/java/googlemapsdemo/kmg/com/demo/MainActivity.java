package googlemapsdemo.kmg.com.demo;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import googlemapsdemo.kmg.com.demo.utils.Util;

public class MainActivity extends AppCompatActivity implements LocationListener,OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleMap.OnMapClickListener {
    Button continueBtn,viewIssue;
    @BindView(R.id.toolbar_title)
    TextView mTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.text_selected)
    TextView textSelected;
    @BindView(R.id.location_icon)
    ImageView locationIcon;

    android.app.AlertDialog alert;
    private LocationManager locationManager;
    private static final long LOCATION_REFRESH_TIME = 500;
    private double sourceLat, sourceLong, destLat, destLong, currentLat, currentLong;
    private Geocoder geocoder;
    List<Address> addresses;
    public String strAddress;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 100;
    private GoogleMap mMap;
    private List<LatLng> latLngList;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    ArgbEvaluator argbEvaluator;
    private Location mLastLocation;
    private List<LatLng> listLatLng = new ArrayList<>();
    private double latitudeValue = 0.0;
    private double longitudeValue = 0.0;
    private MarkerOptions sourceLocationMarker, destinationLocationMarker;
    Marker marker;
    boolean checkGPS = false;
    boolean checkNetwork = false;
    boolean canGetLocation = false;
    Location loc;
    double latitude;
    double longitude;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    private AlertDialog alertDialog;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        mTitle.setText(getString(R.string.location_string));
        mTitle.setTypeface(null, Typeface.BOLD);
        continueBtn = findViewById(R.id.Continue);
        viewIssue = findViewById(R.id.viewIssue);
        textSelected.setVisibility(View.GONE);
//        if(getSupportFragmentManager().getBackStackEntryCount() != 0){
//            getSupportFragmentManager().
//        }
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver((gpsReceiver), new IntentFilter("GPS"));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mLocationRequest = createLocationRequest();
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivty.class);
                intent.putExtra("whichButton","add");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        viewIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivty.class);
                intent.putExtra("whichButton","view");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
       // mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
       // mMap.setOnMyLocationClickListener(onMyLocationClickListener);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            enableMyLocationIfPermitted();
        } else {
            showGPSDisabledAlertToUser();
        }
//        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMinZoomPreference(11);
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    public void showGPSDisabledAlertToUser() {
        if (alert != null) {
            alert.dismiss();
        }
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);

        alertDialogBuilder.setMessage("GPS is disabled. Please enable your GPS to continue.")
                .setCancelable(false)
                .setPositiveButton(Html.fromHtml("<font color='#FB5D6A'>Turn GPS ON</font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = alertDialogBuilder.create();
        alert.setCancelable(false);
        alert.show();
    }

    private void enableMyLocationIfPermitted() {
        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        ) {
            showPermissionAlert(true);

        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            GetMyLocation getMyLocation=new GetMyLocation(this);
            getMyLocation.getCurrentLocationParam();
            //Location location = callProvide();
            currentLat = getMyLocation.sourceLat;
            currentLong = getMyLocation.sourceLong;
            LatLng latLng = new LatLng(currentLat, currentLong);

            marker = mMap.addMarker(new MarkerOptions().position(latLng).title("current location"));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            try {
                Geocoder geo = new Geocoder(MainActivity.this.getApplicationContext(),Locale.getDefault());
                List<Address> addresses = geo.getFromLocation(currentLat, currentLong,1);
                if (addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    Util.addressUser = address;
                    //Toast.makeText(getApplicationContext(),"Address:- " + address, Toast.LENGTH_LONG).show();
                    text.setText(address);
                }
            } catch (Exception e) {
                e.printStackTrace(); // getFromLocation() may sometimes fail
            }
        }
    }


    private BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showGPSDisabledAlertToUser();
                } else {
                    Util.dismissGPSDisabledAlertToUser();
                    enableMyLocationIfPermitted();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    private Location callProvide() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        LocationListener mlocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                currentLat = location.getLatitude();
                currentLong = location.getLongitude();
                LatLng latLng = new LatLng(currentLat, currentLong);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                //Animating the camera
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                1, mlocationListener);

//        locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void showDefaultLocation() {
       // Toast.makeText(this, "Location permission not granted, " +"showing default location",Toast.LENGTH_SHORT).show();
        LatLng redmond = new LatLng(47.6739881, -122.121512);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(redmond));
    }





//        private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
//                new GoogleMap.OnMyLocationButtonClickListener() {
//                    @Override
//                    public boolean onMyLocationButtonClick() {
//                        mMap.setMinZoomPreference(15);
//                        return false;
//                    }
//                };
//
//        private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
//                new GoogleMap.OnMyLocationClickListener() {
//                    @Override
//                    public void onMyLocationClick(@NonNull Location location) {
//
//                        mMap.setMinZoomPreference(12);
//
//                        CircleOptions circleOptions = new CircleOptions();
//                        circleOptions.center(new LatLng(location.getLatitude(),
//                                location.getLongitude()));
//
//                        circleOptions.radius(200);
//                        circleOptions.fillColor(Color.RED);
//                        circleOptions.strokeWidth(6);
//
//                        mMap.addCircle(circleOptions);
//                    }
//                };

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        try {
                            Log.d("GoogleApi", "Connection method has been called");
                            if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                    && ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                                assignLocationValues(mLastLocation);
//                            if (yourLocationMarker != null)
//                                setDefaultMarkerOption(yourLocationMarker.getPosition());
                            } else {
//                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_REQUEST_CODE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }
    private void assignLocationValues(Location currentLocation) {
        if (currentLocation != null) {
            latitudeValue = currentLocation.getLatitude();
            longitudeValue = currentLocation.getLongitude();
            Log.d("GoogleApi2", "Latitude: " + latitudeValue + " Longitude: " + longitudeValue);
            addCameraToMap(new LatLng(latitudeValue, longitudeValue));
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng marker : listLatLng) {
            builder.include(marker);
        }
        LatLngBounds bounds = builder.build();
        int padding = 260; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);
    }
    private void addCameraToMap(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(25)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    @Override
    public void onMapClick(LatLng latLng) {
        if (marker != null) {
            marker.remove();
        }
        marker =mMap.addMarker(new MarkerOptions().position(latLng).title("Custom location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        marker.getPosition();
        MarkerOptions options = new MarkerOptions();
        options.position(latLng);
        double touchLat=latLng.latitude;
        double touchLong=latLng.longitude;
        try {
            Geocoder geo = new Geocoder(MainActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(touchLat,touchLong, 1);
            if (addresses.isEmpty()) {
               // Toast.makeText(getApplicationContext(),"Waiting for Location",Toast.LENGTH_SHORT).show();
            }
            else {

                if (addresses.size() > 0) {
                    String address =addresses.get(0).getAddressLine(0);
                    Util.addressUser=address;
                 //   Toast.makeText(getApplicationContext(), "Address:- " +address, Toast.LENGTH_LONG).show();
                    text.setText(address);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }
    }
    @Override
    protected void onDestroy() {
        mGoogleApiClient.disconnect();
        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(gpsReceiver);
        super.onDestroy();
    }
    public  void showPermissionAlert(boolean isForcedPermissions) {
        if (alertDialog != null && alertDialog.isShowing()) {
            return;
        }
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setTitle(R.string.permission_request_title);
        builder.setMessage(R.string.app_permission_notice);
        builder.create();
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isForcedPermissions) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                    enableMyLocationIfPermitted();
                }
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                enableMyLocationIfPermitted();
                // Toast.makeText(MainActivity.this, Rpermission_refused.string., Toast.LENGTH_LONG).show();
            }
        });
//        builder.show();
        alertDialog = builder.create();
        alertDialog.show();
    }
}
