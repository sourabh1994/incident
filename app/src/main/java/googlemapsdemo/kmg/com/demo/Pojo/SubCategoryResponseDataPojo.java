package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryResponseDataPojo {
    @SerializedName("SubCategoryID")
    @Expose
    private long SubCategoryID;

    @SerializedName("CategoryID")
    @Expose
    private long CategoryID;

    @SerializedName("SubCategoryName")
    @Expose
    private String SubCategoryName;

    @SerializedName("PhoneNumber")
    @Expose
    private String PhoneNumber;

    @SerializedName("Email")
    @Expose
    private String Email;

    @SerializedName("IsActive")
    @Expose
    private boolean IsActive;

    @SerializedName("Icon")
    @Expose
    private String Icon;

    public long getSubCategoryID() {
        return SubCategoryID;
    }

    public void setSubCategoryID(long subCategoryID) {
        SubCategoryID = subCategoryID;
    }

    public long getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(long categoryID) {
        CategoryID = categoryID;
    }

    public String getSubCategoryName() {
        return SubCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        SubCategoryName = subCategoryName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"SubCategoryID\": " + SubCategoryID + ",\n" +
                "  \"CategoryID\": " + CategoryID + ",\n" +
                "  \"SubCategoryName\": \"" + SubCategoryName + "\",\n" +
                "  \"PhoneNumber\": \"" + PhoneNumber + "\",\n" +
                "  \"SubCategoryName\": \"" + SubCategoryName + "\",\n" +
                "  \"IsActive\": " + IsActive + ",\n" +
                "  \"Icon\": \"" + Icon + "\"\n" +
                "}";
    }
}
