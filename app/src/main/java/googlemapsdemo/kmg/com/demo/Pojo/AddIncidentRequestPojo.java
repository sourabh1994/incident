package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddIncidentRequestPojo {

    @SerializedName("UserID")
    @Expose
    private long UserID;

    @SerializedName("SubCategoryID")
    @Expose
    private long SubCategoryID;

    @SerializedName("Location")
    @Expose
    private String Location;

    @SerializedName("JibeStreamID")
    @Expose
    private long JibeStreamID;

    @SerializedName("StatusID")
    @Expose
    private long StatusID;

    @SerializedName("Latitude")
    @Expose
    private double Latitude;

    @SerializedName("Longitude")
    @Expose
    private double Longitude;

    @SerializedName("Description")
    @Expose
    private String Description;

    public long getUserID() {
        return UserID;
    }

    public void setUserID(long userID) {
        UserID = userID;
    }

    public long getSubCategoryID() {
        return SubCategoryID;
    }

    public void setSubCategoryID(long subCategoryID) {
        SubCategoryID = subCategoryID;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public long getJibeStreamID() {
        return JibeStreamID;
    }

    public void setJibeStreamID(long jibeStreamID) {
        JibeStreamID = jibeStreamID;
    }

    public long getStatusID() {
        return StatusID;
    }

    public void setStatusID(long statusID) {
        StatusID = statusID;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"UserID\": " + UserID + ",\n" +
                "  \"SubCategoryID\": " + SubCategoryID + ",\n" +
                "  \"Location\": \"" + Location + "\",\n" +
                "  \"JibeStreamID\": " + JibeStreamID + ",\n" +
                "  \"StatusID\": " + StatusID + ",\n" +
                "  \"Latitude\": " + Latitude + ",\n" +
                "  \"Longitude\": " + Longitude + ",\n" +
                "  \"Description\": \"" + Description + "\"\n" +
                "}";
    }
}
