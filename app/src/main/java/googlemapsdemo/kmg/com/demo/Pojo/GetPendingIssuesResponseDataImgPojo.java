package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPendingIssuesResponseDataImgPojo {

    @SerializedName("url")
    @Expose
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"url\": \"" + url + "\"\n" +
                "}";
    }
}
