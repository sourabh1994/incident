package googlemapsdemo.kmg.com.demo.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import googlemapsdemo.kmg.com.demo.LoginActivity;
import googlemapsdemo.kmg.com.demo.Pojo.IssuePojo;
import googlemapsdemo.kmg.com.demo.R;


public class Util {
    private static ProgressDialog dialog;
    private static Activity nt_act;
    public static String sharedPrefUser = "UserDetails";
    public static String userId = "userId";
    public static String usreLoginStatus = "false";
    public static String addressUser = "";
    public static boolean isFormSubmitted = false;
    public static List<IssuePojo> issuePojos = new ArrayList<>();
    static androidx.appcompat.app.AlertDialog alertDialog;

    public static boolean isNetworkAvailable(Activity activity) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void internetAlert(Activity activity) {
        try {
            showDialogue(activity, "", activity.getString(R.string.check_internet_connection));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void invalidatePicasso(String strUrl) {
        try {
            Picasso.get().invalidate(strUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showProgressDialogue(Activity activity, String title, String message) {
        if (dialog != null && dialog.isShowing()) {
            try {
//                dialog.dismiss();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dialog = new ProgressDialog(activity);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        if (!(activity).isFinishing())
            dialog.show();
    }

    public static void dismissProgressDialogue() {
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }





    public static Dialog dialogNetwork;

    public static void showDialogue(Activity act, String strTitle, String strMessage) {


        dialogNetwork = new Dialog(act);
        dialogNetwork.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNetwork.setCancelable(false);
        dialogNetwork.setContentView(R.layout.dialogue_internet_alert);
        dialogNetwork.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView title = dialogNetwork.findViewById(R.id.title);
        TextView message = dialogNetwork.findViewById(R.id.message);
//        title.setText(strTitle);
        message.setText(strMessage);

        Button read_btn = dialogNetwork.findViewById(R.id.read_btn);
        Button btn_no = dialogNetwork.findViewById(R.id.btn_no);

        read_btn.setText(act.getString(R.string.btn_ok));
        btn_no.setVisibility(View.GONE);

        read_btn.setOnClickListener(v -> {
            dialogNetwork.dismiss();
            if (dialogNetwork.isShowing())
            {
                dialogNetwork.dismiss();
            }
            if (Util.isNetworkAvailable(act)) {
                dialogNetwork.dismiss();
            } else {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                act.startActivity(intent);
            }

        });
        btn_no.setOnClickListener(v -> dialogNetwork.dismiss());

        dialogNetwork.show();
    }


    public static void hideKeyboardFrom(Activity activity, View view) {
        try {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideKeyboardFrom(Context activity, View view) {
        try {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getSelectionPosition(String[] stringArray, String strText) {
        int intGender = 0;
        for (String st : stringArray) {
            if (st.equalsIgnoreCase(strText)) {
                return intGender;
            }
            intGender += 1;
        }
        return 0;
    }

    public static void alertServerIssue(Activity activity) {
        try {
            Toast.makeText(activity, activity.getString(R.string.server_not_responding), Toast.LENGTH_LONG).show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static AlertDialog alert;

    public static void showGPSDisabledAlertToUser(Activity activity) {
        if (alert != null) {
            alert.dismiss();
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

        alertDialogBuilder.setMessage("GPS is disabled. Please enable your GPS to continue.")
                .setCancelable(false)
                .setPositiveButton(Html.fromHtml("<font color='#FB5D6A'>Turn GPS ON</font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = alertDialogBuilder.create();
        alert.setCancelable(false);
        alert.show();
    }

    public static void dismissGPSDisabledAlertToUser() {
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
        }
    }



    public static String formatFare(String strFare) {
        try {
            double x = Double.parseDouble(strFare);
            String strPrice = String.valueOf(Math.floor(x * 100) / 100);
            String[] strTemp = strPrice.split("\\.");
            try {
                if (strTemp.length == 2 && strTemp[1].length() == 1) {
                    return strPrice + "0";
                } else if (strTemp.length == 1) {
                    return strPrice + ".00";
                } else {
                    return strPrice;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return strPrice;
            }
//            return new DecimalFormat("####.##").format(Double.valueOf(strFare));
        } catch (Exception e) {
            e.printStackTrace();
            return strFare;
        }
    }
    public static void enableMyLocationIfPermitted(Context context) {
        if ((ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(((Activity)context), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CAMERA}, 100);

        } else {
            Intent intent = new Intent(context, LoginActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            ((Activity)context).finish();
        }
    }

    public static void showPermissionAlert(boolean isForcedPermissions,Context context) {
        if (alertDialog != null && alertDialog.isShowing()) {
            return;
        }
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder.setTitle(R.string.permission_request_title);
        builder.setMessage(R.string.app_permission_notice);
        builder.create();
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isForcedPermissions) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                    intent.setData(uri);
                    context.startActivity(intent);
                    enableMyLocationIfPermitted(context);
                } else {
                    enableMyLocationIfPermitted(context);
                }
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
                // Toast.makeText(MainActivity.this, Rpermission_refused.string., Toast.LENGTH_LONG).show();
            }
        });
//        builder.show();
        alertDialog = builder.create();
        alertDialog.show();
    }

}
