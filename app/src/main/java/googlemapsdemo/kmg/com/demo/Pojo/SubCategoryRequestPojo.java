package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryRequestPojo {
    @SerializedName("UserId")
    @Expose
    private long UserId;

    @SerializedName("CategoryId")
    @Expose
    private long CategoryId;

    public long getUserId() {
        return UserId;
    }

    public void setUserId(long userId) {
        UserId = userId;
    }

    public long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(long categoryId) {
        CategoryId = categoryId;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"UserId\": \" + UserId + \",\n" +
                "  \"CategoryId\": \" + CategoryId + \"\n" +
                "}";
    }
}

