package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddImagePojo {

    @SerializedName("Image")
    @Expose
    private String Image;


    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"Image\": \"" + Image + "\"\n" +
                "}";
    }
}
