package googlemapsdemo.kmg.com.demo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import googlemapsdemo.kmg.com.demo.Pojo.AddImagePojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddImageRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddImageResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddIncidentResponsePojo;
import googlemapsdemo.kmg.com.demo.R;

public class AddImageAdapter extends RecyclerView.Adapter<AddImageAdapter.MyViewHolder> {

    Context context;
    List<String> addImagePojos;
    public AddImageAdapter(Context context, List<String> addImagePojos) {
        this.context = context;
        this.addImagePojos = addImagePojos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_image_recycler_view_content, parent, false);

        return new AddImageAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String addImagePojo = addImagePojos.get(position);

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.imageClicked);
        }
    }

}
