package googlemapsdemo.kmg.com.demo.utils;

/**
 * Created by Umesh Kumar on 5/15/19.
 */
public class AppUri {



    // New base url
  //  public static final String Production = "http://10.130.205.47:8383/api/"; // live QA: 8092
    public static final String Production = "http://del.kmgin.com:9020/IncidentReporting/api/"; // live QA: 8092




    // New url's
    public static final String authenticateUser= Production  + "Users/AuthenticateUser";
    public static final String getCategories= Production  + "categories/Getcategories";
    public static final String getSubCategories= Production  + "Categories/GetSubCategories";
    public static final String addIncidentRequest= Production  + "Incident/AddIncidentRequest";
    public static final String addImage= Production  + "Incident/AddImage";
    public static final String getPendingIssues= Production  + "Incident/GetPendingIssues";
    public static final String getParameterData= Production  + "Parameters/GetParameterData";




}
