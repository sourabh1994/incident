package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryRequestPojo {

    @SerializedName("UserID")
    @Expose
    private long UserID;

    public long getUserID() {
        return UserID;
    }

    public void setUserID(long userID) {
        UserID = userID;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"UserID\": " + UserID + "\n" +
                "}";
    }
}
