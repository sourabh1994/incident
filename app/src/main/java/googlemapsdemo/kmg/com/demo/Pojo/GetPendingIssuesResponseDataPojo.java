package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPendingIssuesResponseDataPojo {

    @SerializedName("IncidentRequestID")
    @Expose
    private long IncidentRequestID;

    @SerializedName("CategoryID")
    @Expose
    private long CategoryID;

    @SerializedName("SubCategoryID")
    @Expose
    private long SubCategoryID;

    @SerializedName("CategoryName")
    @Expose
    private String CategoryName;

    @SerializedName("SubCategoryName")
    @Expose
    private String SubCategoryName;

    @SerializedName("SubCategoryIcon")
    @Expose
    private String SubCategoryIcon;

    @SerializedName("Description")
    @Expose
    private String Description;

    @SerializedName("Location")
    @Expose
    private String Location;

    @SerializedName("images")
    @Expose
    private List<GetPendingIssuesResponseDataImgPojo> images;

    public long getIncidentRequestID() {
        return IncidentRequestID;
    }

    public void setIncidentRequestID(long incidentRequestID) {
        IncidentRequestID = incidentRequestID;
    }

    public long getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(long categoryID) {
        CategoryID = categoryID;
    }

    public long getSubCategoryID() {
        return SubCategoryID;
    }

    public void setSubCategoryID(long subCategoryID) {
        SubCategoryID = subCategoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getSubCategoryName() {
        return SubCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        SubCategoryName = subCategoryName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public List<GetPendingIssuesResponseDataImgPojo> getImages() {
        return images;
    }

    public void setImages(List<GetPendingIssuesResponseDataImgPojo> images) {
        this.images = images;
    }

    public String getSubCategoryIcon() {
        return SubCategoryIcon;
    }

    public void setSubCategoryIcon(String subCategoryIcon) {
        SubCategoryIcon = subCategoryIcon;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"IncidentRequestID\": " + IncidentRequestID + ",\n" +
                "  \"CategoryID\": " + CategoryID + ",\n" +
                "  \"SubCategoryID\": " + SubCategoryID + ",\n" +
                "  \"CategoryName\": \"" + CategoryName + "\",\n" +
                "  \"SubCategoryName\": \"" + SubCategoryName + "\",\n" +
                "  \"SubCategoryIcon\": \"" + SubCategoryIcon + "\",\n" +
                "  \"Description\": \"" + Description + "\",\n" +
                "  \"Location\": \"" + Location + "\",\n" +
                "  \"images\": \"" + images + "\"\n" +
                "}";
    }

}
