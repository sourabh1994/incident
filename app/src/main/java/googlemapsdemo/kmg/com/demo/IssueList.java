package googlemapsdemo.kmg.com.demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import googlemapsdemo.kmg.com.demo.Adapter.CategoryAdapter;
import googlemapsdemo.kmg.com.demo.Adapter.IssueAdapter;
import googlemapsdemo.kmg.com.demo.Pojo.GetPendingIssuesRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetPendingIssuesResponseDataPojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetPendingIssuesResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryResponsePojo;
import googlemapsdemo.kmg.com.demo.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueList extends Fragment {

    View rootView;
    IssueAdapter issueAdapter;
    RecyclerView recyclerView;
    List<GetPendingIssuesResponseDataPojo> getPendingIssuesResponseDataPojos =new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_issue_list, container, false);
        ((SecondActivty) getActivity()).mTitle.setText("Reported Issues");
        ((SecondActivty) getActivity()).locationIcon.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).text.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).textSelected.setVisibility(View.GONE);
        issueAdapter=new IssueAdapter(getPendingIssuesResponseDataPojos,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerIssue);
        final Animation animFadein = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animFadein.setDuration(1000);
        recyclerView.startAnimation(animFadein);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(issueAdapter);
        recyclerView.clearOnChildAttachStateChangeListeners();
        getPendingIssue();
        issueAdapter.notifyDataSetChanged();
        return rootView;
    }

    private void getPendingIssue() {
        Util.showProgressDialogue(getActivity(),"","");
        SharedPreferences sharedPreferencesUser = getActivity().getSharedPreferences(Util.sharedPrefUser, Context.MODE_PRIVATE);
        GetPendingIssuesRequestPojo getPendingIssuesRequestPojo = new GetPendingIssuesRequestPojo();
        getPendingIssuesRequestPojo.setUserID(sharedPreferencesUser.getLong(Util.userId,0));
        ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
        Call<GetPendingIssuesResponsePojo> call = apiService.getPendingIssues(getPendingIssuesRequestPojo);
        call.enqueue(new Callback<GetPendingIssuesResponsePojo>() {
            @Override
            public void onResponse(Call<GetPendingIssuesResponsePojo> call, Response<GetPendingIssuesResponsePojo> response) {
                Util.dismissProgressDialogue();
                GetPendingIssuesResponsePojo getPendingIssuesResponsePojo =response.body();
                if(response.isSuccessful()){
                    if(getPendingIssuesResponsePojo.getData()!=null && getPendingIssuesResponsePojo.getData().size()>0){
                        getPendingIssuesResponseDataPojos.clear();
                        for (int i = 0 ; i<getPendingIssuesResponsePojo.getData().size();i++){
                            getPendingIssuesResponseDataPojos.add(getPendingIssuesResponsePojo.getData().get(i));
                        }
                        issueAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetPendingIssuesResponsePojo> call, Throwable t) {
                Util.dismissProgressDialogue();
            }
        });

    }

}
