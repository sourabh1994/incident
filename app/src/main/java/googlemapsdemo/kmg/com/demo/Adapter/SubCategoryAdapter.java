package googlemapsdemo.kmg.com.demo.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import googlemapsdemo.kmg.com.demo.Form;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryPojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryResponseDataPojo;
import googlemapsdemo.kmg.com.demo.R;
import googlemapsdemo.kmg.com.demo.utils.Util;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder>{

    private List<SubCategoryResponseDataPojo> dataList;
    Context context;
    String category;

    public SubCategoryAdapter(@NonNull List<SubCategoryResponseDataPojo> dataList, Context context,String category) {
        this.dataList = dataList;
        this.context=context;
        this.category=category;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_content, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        SubCategoryResponseDataPojo data = dataList.get(i);
        myViewHolder.title.setText(data.getSubCategoryName());
//        //myViewHolder.subTitle.setText(data.getSubtitle());
//        Resources res = context.getResources();
//        TypedArray images= res.obtainTypedArray(R.array.subCategory);
//        Drawable drawable = images.getDrawable(i);
//        myViewHolder.imageView.setImageDrawable(drawable);
        Util.invalidatePicasso(data.getIcon());
        String url = data.getIcon();
        Picasso.get().load(url).into(myViewHolder.imageView);
        myViewHolder.categoryCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm= ((AppCompatActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putString("whichSubCategory", myViewHolder.title.getText().toString());
                bundle.putString("whichCategory", category);
                bundle.putLong("SubCategoryId", dataList.get(i).getSubCategoryID());
                Form form=new Form();
                form.setArguments(bundle);
                fragmentTransaction.replace(R.id.container,form, "form");
                fragmentTransaction.addToBackStack("form");
                fragmentTransaction.commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,subTitle;
        public LinearLayout categoryCard;
        public ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            subTitle = (TextView) itemView.findViewById(R.id.sub_title);
            title = (TextView) itemView.findViewById(R.id.title);
            categoryCard = (LinearLayout) itemView.findViewById(R.id.CategoryCard);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
