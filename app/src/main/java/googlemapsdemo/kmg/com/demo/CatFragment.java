package googlemapsdemo.kmg.com.demo;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import googlemapsdemo.kmg.com.demo.Adapter.CategoryAdapter;
import googlemapsdemo.kmg.com.demo.Pojo.CategoryRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.CategoryResponseDataPojo;
import googlemapsdemo.kmg.com.demo.Pojo.CategoryResponsePojo;
import googlemapsdemo.kmg.com.demo.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CatFragment extends Fragment {


    View rootview;
    CategoryAdapter categoryAdapter;
    RecyclerView recyclerView;
    TextView noData;
    private List<CategoryResponseDataPojo> categoryPojos = new ArrayList<>();

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragment_cat, container, false);

        ((SecondActivty) getActivity()).mTitle.setText(getString(R.string.category_text));
        ((SecondActivty) getActivity()).mTitle.setTypeface(null, Typeface.BOLD);
        ((SecondActivty) getActivity()).textSelected.setText(getString(R.string.select_category));
        ((SecondActivty) getActivity()).locationIcon.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).text.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).textSelected.setVisibility(View.VISIBLE);
        categoryAdapter = new CategoryAdapter(categoryPojos, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerCategory);
        noData = (TextView) rootview.findViewById(R.id.noData);
        final Animation animFadein = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animFadein.setDuration(1000);
        recyclerView.startAnimation(animFadein);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(categoryAdapter);
        recyclerView.clearOnChildAttachStateChangeListeners();
        prepareNotiCategoryPojo();

        return rootview;
    }

    private void prepareNotiCategoryPojo() {
        Util.showProgressDialogue(getActivity(), "", "");
        SharedPreferences sharedPreferencesUser = getActivity().getSharedPreferences(Util.sharedPrefUser, Context.MODE_PRIVATE);
        CategoryRequestPojo categoryRequestPojo = new CategoryRequestPojo();
        categoryRequestPojo.setUserID(sharedPreferencesUser.getLong(Util.userId, 0));
        ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
        Call<CategoryResponsePojo> call = apiService.getCategories(categoryRequestPojo);
        call.enqueue(new Callback<CategoryResponsePojo>() {
            @Override
            public void onResponse(Call<CategoryResponsePojo> call, Response<CategoryResponsePojo> response) {
                CategoryResponsePojo categoryResponsePojo = response.body();
                Util.dismissProgressDialogue();
                if (response.isSuccessful() && categoryResponsePojo.getData() != null && categoryResponsePojo.getData().size() > 0) {
                    categoryPojos.clear();
                    for (int i = 0; i < categoryResponsePojo.getData().size(); i++) {
                        categoryPojos.add(categoryResponsePojo.getData().get(i));
                    }
                    categoryAdapter.notifyDataSetChanged();
                } else {
                    recyclerView.setVisibility(View.GONE);
                    noData.setText("No data found!");
                    noData.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<CategoryResponsePojo> call, Throwable t) {
                Util.dismissProgressDialogue();
            }
        });


    }
//    private void prepareNotiCategoryPojo() {
//
//            categoryPojos.clear();
//
//            CategoryPojo categoryPojosNotification = new CategoryPojo("Food","Fix of assemble");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new CategoryPojo("Pharmacy","Keeping it tidy");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new CategoryPojo("IT","Don't shock yourself");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new CategoryPojo("Mechanic","Fix of assemble");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new CategoryPojo("Electrical","Don't shock yourself");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new CategoryPojo("Cleaning","No more leaks");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new CategoryPojo("Maintenance","No more leaks");
//            categoryPojos.add(categoryPojosNotification);
//
//            categoryAdapter.notifyDataSetChanged();
//
//    }

}
