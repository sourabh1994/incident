package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetParameterResponseDataPojo {
    @SerializedName("ParameterID")
    @Expose
    private double ParameterID;

    @SerializedName("ParameterKey")
    @Expose
    private String ParameterKey;

    @SerializedName("ParameterValue")
    @Expose
    private String ParameterValue;

    public double getParameterID() {
        return ParameterID;
    }

    public void setParameterID(double parameterID) {
        ParameterID = parameterID;
    }

    public String getParameterKey() {
        return ParameterKey;
    }

    public void setParameterKey(String parameterKey) {
        ParameterKey = parameterKey;
    }

    public String getParameterValue() {
        return ParameterValue;
    }

    public void setParameterValue(String parameterValue) {
        ParameterValue = parameterValue;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"ParameterID\": " + ParameterID + ",\n" +
                "  \"ParameterKey\": \"" + ParameterKey + "\",\n" +
                "  \"ParameterValue\": \"" + ParameterValue + "\"\n" +
                "}";
    }
}
