package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddImageRequestPojo {
    @SerializedName("FileName")
    @Expose
    private String FileName;

    @SerializedName("IncidentRequestID")
    @Expose
    private long IncidentRequestID;

    @SerializedName("Image")
    @Expose
    private String Image;

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public long getIncidentRequestID() {
        return IncidentRequestID;
    }

    public void setIncidentRequestID(long incidentRequestID) {
        IncidentRequestID = incidentRequestID;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"FileName\":\"" + FileName + "\",\n" +
                "  \"IncidentRequestID\": " + IncidentRequestID + ",\n" +
                "  \"Image\": \"" + Image + "\"\n" +
                "}";
    }
}
