package googlemapsdemo.kmg.com.demo.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import googlemapsdemo.kmg.com.demo.R;

public class DialogueView
{
        Button read_btn, btn_no;
        LinearLayout linearLayout;


        public void showDialog(final Activity activity, String strTitle, String strMessage, int position) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            switch (position) {
                case 0:
                    read_btn.setText(activity.getString(R.string.btn_ok));
                    btn_no.setText(activity.getString(R.string.btn_cancel));
                    btn_no.setVisibility(View.VISIBLE);
                    break;
            }

            read_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   switch (position) {
                        case 0:
                            dialog.dismiss();
                            break;

                    }
                }

            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.setCancelable(false);
            dialog.show();
        }

    }




