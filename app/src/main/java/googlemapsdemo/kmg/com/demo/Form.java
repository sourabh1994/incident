package googlemapsdemo.kmg.com.demo;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import googlemapsdemo.kmg.com.demo.Pojo.AddImageRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddImageResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddIncidentRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddIncidentResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetParameterDataRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetParameterResponseDataPojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetParameterResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.IssuePojo;
import googlemapsdemo.kmg.com.demo.utils.Base64Converter;
import googlemapsdemo.kmg.com.demo.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.media.MediaRecorder.VideoSource.CAMERA;

public class Form extends Fragment {

    View rootview;
    Unbinder unbinder;
    @BindView(R.id.enter_note)
    EditText enterNote;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.img_camera_icon)
    Button imgCameraIcon;
    @BindView(R.id.makeACall)
    Button makeACall;
    RecyclerView recyclerview;
    private AlertDialog.Builder builder;
    private CharSequence[] options;
    String picturePath;
    private Uri uri;
    private static final String IMAGE_DIRECTORY = "/incident/";
    private AlertDialog alertDialog;
    public int camera_called_count;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    Bundle bundle;
    IssuePojo issuePojo;
    int success = 0;
    SharedPreferences sharedPreferencesUser;
    private List<String> listOfImagesPath;
    private static int count = 0;
    private GridView grid;
    private List<String> listOfImagesPathOld;
    private List<String> listOfImages=new ArrayList<String>();
    int noOfImagesAllowed;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragment_form, container, false);
        unbinder = ButterKnife.bind(this, rootview);
        Util.isFormSubmitted = false;
        grid = rootview.findViewById(R.id.gridviewimg);

        sharedPreferencesUser = getActivity().getSharedPreferences(Util.sharedPrefUser, Context.MODE_PRIVATE);
        options = getActivity().getResources().getStringArray(R.array.cameraOptions);


        camera_called_count = 0;
        bundle = getArguments();

        GetParameterDataRequestPojo getParameterDataRequestPojo=new GetParameterDataRequestPojo();
        getParameterDataRequestPojo.setUserID(sharedPreferencesUser.getLong(Util.userId, 0));
        getParameterDataRequestPojo.setParameterKey("IncidentImageCount");
        ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
        Call<GetParameterResponsePojo> call = apiService.getParameterData(getParameterDataRequestPojo);
        call.enqueue(new Callback<GetParameterResponsePojo>() {
            @Override
            public void onResponse(Call<GetParameterResponsePojo> call, Response<GetParameterResponsePojo> response) {
                GetParameterResponsePojo getParameterResponsePojo= response.body();
                if(response.isSuccessful() && getParameterResponsePojo.getData()!=null && getParameterResponsePojo.getData().size()>0){
                    if(getParameterDataRequestPojo.getParameterKey().equalsIgnoreCase("IncidentImageCount")){
                        noOfImagesAllowed= Integer.parseInt(getParameterResponsePojo.getData().get(0).getParameterValue());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetParameterResponsePojo> call, Throwable t) {
                t.printStackTrace();
            }
        });


        GetMyLocation getMyLocation = new GetMyLocation(getActivity());
        getMyLocation.getCurrentLocationParam();

        listOfImagesPathOld = RetriveCapturedImagePathold();
        if (listOfImagesPathOld != null) {
            count = listOfImagesPathOld.size();
        }
        listOfImagesPath = new ArrayList<String>();
        String whichCategory = bundle.getString("whichCategory");
        String whichSubCategory = bundle.getString("whichSubCategory");
//        try {
//
//            ((SecondActivty) getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//
//        } catch (Exception e) {
//
//            ((MainActivity) getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//        }
        ((SecondActivty) getActivity()).mTitle.setText(whichCategory + "/" + whichSubCategory); //to be sent from previous screen
        ((SecondActivty) getActivity()).mTitle.setTypeface(null, Typeface.NORMAL);
        ((SecondActivty) getActivity()).text.setText(Util.addressUser);
        ((SecondActivty) getActivity()).locationIcon.setVisibility(View.VISIBLE);
        ((SecondActivty) getActivity()).textSelected.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).text.setVisibility(View.VISIBLE);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (enterNote.getText().length() > 0) {
                    Util.isFormSubmitted = true;
                    issuePojo = new IssuePojo(whichCategory, whichSubCategory);
                    Util.issuePojos.add(issuePojo);
                    Util.showProgressDialogue(getActivity(), "", getString(R.string.loading));
                    AddIncidentRequestPojo addIncidentRequestPojo = new AddIncidentRequestPojo();
                    addIncidentRequestPojo.setUserID(sharedPreferencesUser.getLong(Util.userId, 0));
                    addIncidentRequestPojo.setSubCategoryID(bundle.getLong("SubCategoryId"));
                    addIncidentRequestPojo.setLocation(getMyLocation.strAddress);
                    addIncidentRequestPojo.setLatitude(getMyLocation.sourceLat);
                    addIncidentRequestPojo.setLongitude(getMyLocation.sourceLong);
                    addIncidentRequestPojo.setJibeStreamID(0);
                    addIncidentRequestPojo.setStatusID(2);
                    addIncidentRequestPojo.setDescription(enterNote.getText().toString());
                    ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
                    Call<AddIncidentResponsePojo> call = apiService.addIncidentRequest(addIncidentRequestPojo);
                    call.enqueue(new Callback<AddIncidentResponsePojo>() {
                        @Override
                        public void onResponse(Call<AddIncidentResponsePojo> call, Response<AddIncidentResponsePojo> response) {
                            AddIncidentResponsePojo addIncidentResponsePojo = response.body();
                            if (response.isSuccessful()) {
                                if (addIncidentResponsePojo.getData() != null) {

                                    for (int i = 0; i < listOfImagesPath.size(); i++) {
                                        if (callAddImgApi(i, addIncidentResponsePojo.getData().getIncidentRequestId())) {
                                            Toast.makeText(getActivity(), "Please try later", Toast.LENGTH_LONG).show();
                                        }
                                    /*else
                                        callAddImgApi(i, addIncidentResponsePojo.getData().getIncidentRequestId());*/
                                    }
                                    showDialogue(getActivity(), "", "Report Submitted");


                                }
                                Util.dismissProgressDialogue();
                            }
                        }

                        @Override
                        public void onFailure(Call<AddIncidentResponsePojo> call, Throwable t) {

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Please provide some description", Toast.LENGTH_LONG).show();
                }

            }


        });

        makeACall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9999999999"));
                startActivity(intent);
            }
        });

        return rootview;

    }



    protected boolean callAddImgApi(int imgNo, long incidentReportId) {
        success = 0;
        AddImageRequestPojo addImageRequestPojo = new AddImageRequestPojo();
        Bitmap thePic = (BitmapFactory.decodeFile(listOfImagesPath.get(imgNo)));
        addImageRequestPojo.setFileName("image" + imgNo);
        addImageRequestPojo.setIncidentRequestID(incidentReportId);
        addImageRequestPojo.setImage(Base64Converter.encodeBitmapIntoBase64(thePic));
        ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
        Call<AddImageResponsePojo> call = apiService.addImage(addImageRequestPojo);
        call.enqueue(new Callback<AddImageResponsePojo>() {
            @Override
            public void onResponse(Call<AddImageResponsePojo> call, Response<AddImageResponsePojo> response) {
                AddImageResponsePojo addImageResponsePojo = response.body();
                if (addImageResponsePojo != null) {
                    if (response.isSuccessful()) {
                        if (addImageResponsePojo.getSuccess().toLowerCase().equalsIgnoreCase("true")) {
                            success = 1;
                        }
                    } else
                        Toast.makeText(getActivity(), "Please try later", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddImageResponsePojo> call, Throwable t) {
                Util.dismissProgressDialogue();
                Toast.makeText(getActivity(), "Please try later", Toast.LENGTH_LONG).show();
            }
        });
        if (success == 1)
            return true;
        else
            return false;

    }

    public boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            showPermissionAlert(true);
            return false;
        }
        return true;
    }

    private void showPermissionAlert(boolean isForcedPermissions) {
        if (alertDialog != null && alertDialog.isShowing()) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Special Permission Request");
        builder.setMessage("Please give camera permission");
        builder.create();
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", (dialog, which) -> {
            if (isForcedPermissions) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                checkCameraPermission();
            } else {
                checkCameraPermission();
            }
            alertDialog.dismiss();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            alertDialog.dismiss();
        });
//        builder.show();
        alertDialog = builder.create();
        alertDialog.show();
    }

    @OnClick(R.id.img_camera_icon)
    public void onViewClicked()
    {
        if (checkCameraPermission())
        {
            if (camera_called_count < noOfImagesAllowed)
            {
                builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Add Photo");
                builder.setItems(options, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int item)
                    {
                        if (options[item].equals("Take Photo"))
                        {
                            activateStrictMode();
                            captureImage(1);

                        }else if (options[item].equals("Choose from Gallery")) {
                            Intent intent1 = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent1, 2);
                        }  else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else{
                Toast.makeText(getActivity(),"You have already added the allowed number of images",Toast.LENGTH_LONG).show();
            }
        }
    }

//    private void takePhotoFromCamera() {
//        recyclerview.setAdapter(adapter);
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, CAMERA);
//
//    }


    private void activateStrictMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


//    private void promptSpeechInput() {
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
//        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
//                getString(R.string.speech_prompt));
//        try {
//            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
//        } catch (ActivityNotFoundException a) {
//            Toast.makeText(getActivity(),
//                    getString(R.string.speech_not_supported),
//                    Toast.LENGTH_SHORT).show();
//        }
//    }

    /**
     * Receiving speech input
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA: {
                if(data!=null)
                {
                    count++;
                    camera_called_count++;
                }
                try {
                    listOfImagesPath=RetriveCapturedImagePath();
                    for(int i = 0 ; i<listOfImagesPath.size();i++)
                        if(!listOfImages.contains(listOfImagesPath.get(i)))
                        {
                            listOfImages.add(listOfImagesPath.get(i));
                        }
                    if (listOfImages != null) {
                        grid.setAdapter(new ImageListAdapter(getActivity(), listOfImages));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }
            case 2:
                if(data!=null)
                {
                    count++;
                    camera_called_count++;
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();
                    listOfImages.add(picturePath);
                    grid.setAdapter(new ImageListAdapter(getActivity(), listOfImages));
                }
                break;
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    enterNote.setText(result.get(0));
                }
                break;
            }
        }
    }

    private List<String> RetriveCapturedImagePath() {
        List<String> tFileList = new ArrayList<String>();
        File f = new File(Environment.getExternalStorageDirectory() + "/incident/");
        if (f.exists()) {
            File[] files = f.listFiles();
            Arrays.sort(files);
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.isDirectory() || (listOfImagesPathOld != null && listOfImagesPathOld.contains(file.getPath())))
                    continue;
                tFileList.add(file.getPath());
            }
        }
        return tFileList;
    }
    private List<String> RetriveCapturedImagePathold() {
        List<String> tFileListOld = new ArrayList<String>();
        File f = new File(Environment.getExternalStorageDirectory() + "/incident/");
        if (f.exists()) {
            File[] files = f.listFiles();
            Arrays.sort(files);
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.isDirectory())
                    continue;
                tFileListOld.add(file.getPath());
            }
        }
        return tFileListOld;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void captureImage(int MY_INTENT_CLICK) {
        // Check Camera
        if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        // Do something for KITKAT versions
                        uri = Uri.fromFile(photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        startActivityForResult(cameraIntent, MY_INTENT_CLICK);
                    } else {
                        uri = FileProvider.getUriForFile(getActivity().getApplicationContext(), BuildConfig.APPLICATION_ID + ".fileProvider", photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        startActivityForResult(cameraIntent, MY_INTENT_CLICK);
                    }
                }
            }

        } else {
            Toast.makeText(getActivity().getApplicationContext(), "Camera not supported", Toast.LENGTH_LONG).show();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
//        s = s.replace("/", "-");
        String imageName = "/IMG" + "_";
        String imageFileName = imageName + count;
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "incident");

        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();


        }
        File file = new File(imagesFolder + imageFileName + ".jpg");
        // Save a file: path for use
        picturePath = file.getAbsolutePath();

//        File fdelete = new File(picturePath);
//        if (fdelete.exists()) {
//            if (fdelete.delete()) {
//                System.out.println("file Deleted :" + picturePath);
//            } else {
//                System.out.println("file not Deleted :" + picturePath);
//            }
//        }

        return file;
    }

    public class ImageListAdapter extends BaseAdapter {
        private Context context;
        private List<String> imgPic;

        public ImageListAdapter(Context c, List<String> thePic) {
            context = c;
            imgPic = thePic;
        }

        public int getCount() {
            if (imgPic != null)
                return imgPic.size();
            else
                return 0;
        }

        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        //---returns an ImageView view---
        public class Holder {
            ImageView cancel;
            ImageView img;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future

            Holder holder = new Holder();
            View rowView;

            rowView = layoutInflater.inflate(R.layout.single_item, null);
            holder.cancel = (ImageView) rowView.findViewById(R.id.button);
            holder.img = (ImageView) rowView.findViewById(R.id.thumbImage);
            holder.cancel.setRotation(0);


            File file = new File(imgPic.get(position));


            holder.img.setLayoutParams(new ConstraintLayout.LayoutParams(350, 360));

            Glide.with(getActivity()).load(file).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(holder.img);

            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    camera_called_count--;
                    listOfImagesPathOld.add(listOfImagesPath.get(position));
                    listOfImagesPath.remove(position);
                    notifyDataSetChanged();

                }
            });

            return rowView;
        }
    }

    public static Dialog dialogNetwork;

    public static void showDialogue(Activity act, String strTitle, String strMessage) {


        dialogNetwork = new Dialog(act);
        dialogNetwork.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNetwork.setCancelable(false);
        dialogNetwork.setContentView(R.layout.dialogue_internet_alert);
        dialogNetwork.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView title = dialogNetwork.findViewById(R.id.title);
        TextView message = dialogNetwork.findViewById(R.id.message);
//        title.setText(strTitle);
        message.setText(strMessage);

        Button read_btn = dialogNetwork.findViewById(R.id.read_btn);
        Button btn_no = dialogNetwork.findViewById(R.id.btn_no);

        read_btn.setText(act.getString(R.string.btn_ok));
        btn_no.setVisibility(View.GONE);

        read_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(act, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                act.startActivity(intent);
                act.finish();
            }
        });
        btn_no.setOnClickListener(v -> dialogNetwork.dismiss());

        dialogNetwork.show();
    }

}
