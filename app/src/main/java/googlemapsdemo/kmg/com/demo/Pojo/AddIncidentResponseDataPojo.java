package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddIncidentResponseDataPojo {

    @SerializedName("IncidentRequestId")
    @Expose
    private long IncidentRequestId;

    public long getIncidentRequestId() {
        return IncidentRequestId;
    }

    public void setIncidentRequestId(long incidentRequestId) {
        IncidentRequestId = incidentRequestId;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"IncidentRequestId\": " + IncidentRequestId + "\n" +
                "}";
    }
}
