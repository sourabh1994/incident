package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetParameterDataRequestPojo {
    @SerializedName("UserID")
    @Expose
    private long UserID;

    @SerializedName("ParameterKey")
    @Expose
    private String ParameterKey;

    public long getUserID() {
        return UserID;
    }

    public void setUserID(long userID) {
        UserID = userID;
    }

    public String getParameterKey() {
        return ParameterKey;
    }

    public void setParameterKey(String parameterKey) {
        ParameterKey = parameterKey;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"UserID\": " + UserID + ",\n" +
                "  \"ParameterKey\": \"" + ParameterKey + "\"\n" +
                "}";
    }
}
