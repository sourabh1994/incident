package googlemapsdemo.kmg.com.demo;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import googlemapsdemo.kmg.com.demo.utils.AppUri;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import static in.gov.rbsk.utils.AppUtils.TIME_OUT_100;

/**
 * Created by Umesh Kumar on 16-04-2018.
 */

public class ApiClient {
    public static final int FILE_TYPE_IMAGE = 1;
    public static final int FILE_TYPE_VIDEO = 2;
    public static final int CAPTURE_IMAGE = 101;
    public static final int TIME_OUT_100 = 100;
    public static final int UNAUTHORIZED = 401;
    public static final int NOT_FOUND = 404;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static String tokenR;

    //QA
//    private static final String BASE_URL = "http://202.153.37.87/";

    //development
    private static final String BASE_URL = "http://202.153.34.167/";
    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder client = new OkHttpClient.Builder()
            .connectTimeout(TIME_OUT_100, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT_100, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT_100, TimeUnit.SECONDS);

//    public static Retrofit getClient() {
//        client.addInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request original = chain.request();
//                Request request = original.newBuilder()
//                        .header("Token", "")
//                        .method(original.method(), original.body())
//                        .build();
//                return chain.proceed(request);
//            }
//        });
//
//        if (retrofit == null) {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(AppUri.ProductionQa)
//                    .client(client.build())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//        }
//        return retrofit;
//    }

    public static Retrofit getClientNew(final String token) {
        tokenR=token;
        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                if (tokenR.equalsIgnoreCase("")) {
                    request = original.newBuilder()
                            .method(original.method(), original.body())
                            .build();
                } else {
                    request = original.newBuilder()
                            .header("Token", tokenR)
                            .method(original.method(), original.body())
                            .build();
                }

                return chain.proceed(request);
            }
        });

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppUri.Production)
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    /*public static Retrofit getSitUrl() {
        retrofit = null;
        retrofit = new Retrofit.Builder()
                .baseUrl(AppUri.Production)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getShivaUrl() {
        retrofit = null;
        retrofit = new Retrofit.Builder()
                .baseUrl(AppUri.shivaUrlNew)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getGeetaUrl() {
        retrofit = null;
        retrofit = new Retrofit.Builder()
                .baseUrl(AppUri.rahulUrl)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }*/


}
