package googlemapsdemo.kmg.com.demo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import googlemapsdemo.kmg.com.demo.Pojo.LoginRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.LoginResponsePojo;
import googlemapsdemo.kmg.com.demo.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.usernameEd)
    EditText usernameEd;
    @BindView(R.id.passwordEd)
    EditText passwordEd;
    @BindView(R.id.show_pass_btn)
    ImageView showPassBtn;
    @BindView(R.id.loginBtn)
    Button loginBtn;
    SharedPreferences sharedPrefUser;
    @BindView(R.id.imageView)
    ImageView imageView;


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        GetMyLocation getMyLocation = new GetMyLocation(this);
        getMyLocation.getCurrentLocationParam();
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        params.gravity= Gravity.CENTER;
//        params.setMargins(0, 10, 0, 0);
        sharedPrefUser = getSharedPreferences(Util.sharedPrefUser, MODE_PRIVATE);
        Util.hideKeyboardFrom(this, getCurrentFocus());

        usernameEd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                usernameEd.setError(null);
                usernameEd.clearFocus();
                passwordEd.setError(null);
                passwordEd.clearFocus();
            }
        });

        usernameEd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    passwordEd.requestFocus();
                }
                return false;
            }
        });
        passwordEd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    loginBtn.callOnClick();
                }
                return false;
            }
        });
        passwordEd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                usernameEd.setError(null);
                usernameEd.clearFocus();
                passwordEd.setError(null);
                passwordEd.clearFocus();
            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.hideKeyboardFrom(LoginActivity.this,getCurrentFocus());
                if (!isValidEmail(usernameEd.getText().toString())) {
                    usernameEd.requestFocus();
                    usernameEd.setError(getString(R.string.correct_Email));
                    return;
                }
                if (passwordEd.getText().toString().equalsIgnoreCase("")) {
                    passwordEd.requestFocus();
                    passwordEd.setError(getString(R.string.password_blank));
                    return;
                }
                Util.showProgressDialogue(LoginActivity.this, "", getString(R.string.loading));
                LoginRequestPojo loginRequestPojo = new LoginRequestPojo();
                loginRequestPojo.setUserName(usernameEd.getText().toString());
                loginRequestPojo.setPassword(passwordEd.getText().toString());
                loginRequestPojo.setLocation(getMyLocation.strAddress);
                loginRequestPojo.setAddress(getMyLocation.strAddress);
                loginRequestPojo.setDeviceType("Android");
                loginRequestPojo.setLatitude(getMyLocation.sourceLat);
                loginRequestPojo.setLongitude(getMyLocation.sourceLong);
                loginRequestPojo.setDeviceToken("NA");
                ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
                Call<LoginResponsePojo> call = apiService.authenticateUser(loginRequestPojo);
                call.enqueue(new Callback<LoginResponsePojo>() {
                    @Override
                    public void onResponse(Call<LoginResponsePojo> call, Response<LoginResponsePojo> response) {
                        LoginResponsePojo loginResponsePojo = response.body();
                        Util.dismissProgressDialogue();
                        if (loginResponsePojo != null) {
                            if (response.isSuccessful()) {
                                if (loginResponsePojo.getData() != null) {
                                    SharedPreferences.Editor editor = sharedPrefUser.edit();
                                    editor.putLong(Util.userId, loginResponsePojo.getData().getUserID());
                                    editor.putString(Util.usreLoginStatus, "true");
                                    editor.commit();
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponsePojo> call, Throwable t) {
                        Util.dismissProgressDialogue();
                    }
                });


            }
        });
        showPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passwordEd.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                    ((ImageView) (view)).setImageResource(R.drawable.password_visible);
                    //Show Password
                    passwordEd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ((ImageView) (view)).setImageResource(R.drawable.password_visible);

                    //Hide Password
                    passwordEd.setTransformationMethod(PasswordTransformationMethod.getInstance());

                }
            }
        });
    }

    public static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
