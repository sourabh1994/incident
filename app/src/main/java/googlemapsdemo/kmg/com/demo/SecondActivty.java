package googlemapsdemo.kmg.com.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import googlemapsdemo.kmg.com.demo.utils.Util;

public class SecondActivty extends AppCompatActivity {
    public FragmentManager fm;
    @BindView(R.id.toolbar_title)
    TextView mTitle;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.text_selected)
    TextView textSelected;
    @BindView(R.id.location_icon)
    ImageView locationIcon;
    @BindView(R.id.hamburger)
    ImageView hamburger;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_activty);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        hamburger.setOnClickListener(v -> onBackPressed());
        fm = getSupportFragmentManager();
        String whichBtn = getIntent().getStringExtra("whichButton");
        FragmentTransaction ft = fm.beginTransaction();
        if(whichBtn.equalsIgnoreCase("add"))
            ft.add(R.id.container, new CatFragment());
        else
            ft.add(R.id.container, new IssueList());

        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//        if(Util.isFormSubmitted==true){
//            getSupportFragmentManager().popBackStack();
//            getSupportFragmentManager().popBackStack();
//        }
        super.onBackPressed();

    }
}
