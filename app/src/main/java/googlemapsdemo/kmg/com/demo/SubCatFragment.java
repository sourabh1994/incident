package googlemapsdemo.kmg.com.demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import googlemapsdemo.kmg.com.demo.Adapter.SubCategoryAdapter;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryResponseDataPojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryResponsePojo;
import googlemapsdemo.kmg.com.demo.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCatFragment extends Fragment {

    View rootview;
    SubCategoryAdapter subcategoryAdapter;
    RecyclerView recyclerView;
    Bundle bundle;
    long categoryId;
    TextView noData;
    private List<SubCategoryResponseDataPojo> subCategoryPojos = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragment_sub_cat, container, false);
        ((SecondActivty) getActivity()).textSelected.setText(getString(R.string.sub_category));
        ((SecondActivty) getActivity()).mTitle.setTypeface(null, Typeface.BOLD);
        ((SecondActivty) getActivity()).locationIcon.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).text.setVisibility(View.GONE);
        ((SecondActivty) getActivity()).textSelected.setVisibility(View.VISIBLE);
        bundle = getArguments();
        String whichCategory = bundle.getString("whichCategory");
        categoryId = bundle.getLong("CategoryId");
        ((SecondActivty) getActivity()).mTitle.setText(whichCategory); //to be sent from previous screen

        subcategoryAdapter = new SubCategoryAdapter(subCategoryPojos, getActivity(), whichCategory);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerSubCategory);
        noData = (TextView) rootview.findViewById(R.id.noData);
        final Animation animFadein = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animFadein.setDuration(1000);
        recyclerView.startAnimation(animFadein);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(subcategoryAdapter);
        recyclerView.clearOnChildAttachStateChangeListeners();
        prepareNotiSubCategoryPojo(categoryId);
        subcategoryAdapter.notifyDataSetChanged();
        return rootview;
    }

    private void prepareNotiSubCategoryPojo(long categoryId) {
        Util.showProgressDialogue(getActivity(), "", "");
        SharedPreferences sharedPreferencesUser = getActivity().getSharedPreferences(Util.sharedPrefUser, Context.MODE_PRIVATE);
        SubCategoryRequestPojo subCategoryRequestPojo = new SubCategoryRequestPojo();
        subCategoryRequestPojo.setUserId(sharedPreferencesUser.getLong(Util.userId, 0));
        subCategoryRequestPojo.setCategoryId(categoryId);
        ApiInterface apiService = ApiClient.getClientNew("").create(ApiInterface.class);
        Call<SubCategoryResponsePojo> call = apiService.getSubCategories(subCategoryRequestPojo);
        call.enqueue(new Callback<SubCategoryResponsePojo>() {
            @Override
            public void onResponse(Call<SubCategoryResponsePojo> call, Response<SubCategoryResponsePojo> response) {
                SubCategoryResponsePojo subCategoryResponsePojo = response.body();
                Util.dismissProgressDialogue();
                if (response.isSuccessful()) {
                    if (subCategoryResponsePojo.getData() != null && subCategoryResponsePojo.getData().size() > 0) {
                        subCategoryPojos.clear();
                        for (int i = 0; i < subCategoryResponsePojo.getData().size(); i++) {
                            subCategoryPojos.add(subCategoryResponsePojo.getData().get(i));
                        }
                        subcategoryAdapter.notifyDataSetChanged();
                    } else {
                            recyclerView.setVisibility(View.GONE);
                            noData.setText("No data found!");
                            noData.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onFailure(Call<SubCategoryResponsePojo> call, Throwable t) {
                Util.dismissProgressDialogue();
            }
        });
    }
//    private void prepareNotiSubCategoryPojo(String whichCategory) {
//        subCategoryPojos.clear();
//       if(whichCategory.toLowerCase().equalsIgnoreCase("food")) {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room","Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store","Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//        }else if(whichCategory.toLowerCase().equalsIgnoreCase("pharmacy")) {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room", "Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store", "Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//        }else if(whichCategory.toLowerCase().equalsIgnoreCase("it")) {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room", "Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store", "Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//        }else if(whichCategory.toLowerCase().equalsIgnoreCase("mechanic")) {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room", "Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store", "Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//        }else if(whichCategory.toLowerCase().equalsIgnoreCase("electrical")) {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room", "Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store", "Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//        }else if(whichCategory.toLowerCase().equalsIgnoreCase("cleaning")) {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room", "Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store", "Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//        }else {
//            SubCategoryPojo categoryPojosNotification = new SubCategoryPojo("Room","Keeping it tgidy");
//            subCategoryPojos.add(categoryPojosNotification);
//
//            categoryPojosNotification = new SubCategoryPojo("Store","Fix of assemble");
//            subCategoryPojos.add(categoryPojosNotification);
//
//
//        }
//        subcategoryAdapter.notifyDataSetChanged();
//
//    }

}
