package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponseDataPojo {

    @SerializedName("UserID")
    @Expose
    private long UserID;

    @SerializedName("UserName")
    @Expose
    private String UserName;

    @SerializedName("Password")
    @Expose
    private String Password;

    @SerializedName("Location")
    @Expose
    private String Location;

    @SerializedName("Latitude")
    @Expose
    private String Latitude;

    @SerializedName("Longitude")
    @Expose
    private String Longitude;

    @SerializedName("Address")
    @Expose
    private String Address;

    @SerializedName("DeviceType")
    @Expose
    private String DeviceType;

    @SerializedName("DeviceToken")
    @Expose
    private String DeviceToken;

    public long getUserID() {
        return UserID;
    }

    public void setUserID(long userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"UserID\": " + UserID + ",\n" +
                "  \"UserName\": \"" + UserName + "\",\n" +
                "  \"Password\": \"" + Password + "\",\n" +
                "  \"Location\": \"" + Location + "\",\n" +
                "  \"Latitude\": \"" + Latitude + "\",\n" +
                "  \"Longitude\": \"" + Longitude + "\",\n" +
                "  \"Address\": \"" + Address + "\",\n" +
                "  \"DeviceType\": \"" + DeviceType + "\",\n" +
                "  \"DeviceToken\": \"" + DeviceToken + "\"\n" +
                "}";
    }

}
