package googlemapsdemo.kmg.com.demo;


import googlemapsdemo.kmg.com.demo.Pojo.AddImageRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddImageResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddIncidentRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.AddIncidentResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.CategoryRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.CategoryResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetParameterDataRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetParameterResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetPendingIssuesRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.GetPendingIssuesResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.LoginRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.LoginResponsePojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryRequestPojo;
import googlemapsdemo.kmg.com.demo.Pojo.SubCategoryResponsePojo;
import googlemapsdemo.kmg.com.demo.utils.AppUri;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {


    @POST(AppUri.authenticateUser)
    Call<LoginResponsePojo> authenticateUser(@Body LoginRequestPojo loginRequestPojo);

    @POST(AppUri.getCategories)
    Call<CategoryResponsePojo> getCategories(@Body CategoryRequestPojo categoryRequestPojo);

    @POST(AppUri.getSubCategories)
    Call<SubCategoryResponsePojo> getSubCategories(@Body SubCategoryRequestPojo subCategoryRequestPojo);

    @POST(AppUri.addIncidentRequest)
    Call<AddIncidentResponsePojo> addIncidentRequest(@Body AddIncidentRequestPojo addIncidentRequest);

    @POST(AppUri.addImage)
    Call<AddImageResponsePojo> addImage(@Body AddImageRequestPojo addImageRequestPojo);

    @POST(AppUri.getPendingIssues)
    Call<GetPendingIssuesResponsePojo> getPendingIssues(@Body GetPendingIssuesRequestPojo getPendingIssuesRequestPojo);

    @POST(AppUri.getParameterData)
    Call<GetParameterResponsePojo> getParameterData(@Body GetParameterDataRequestPojo getParameterDataRequestPojo);

}