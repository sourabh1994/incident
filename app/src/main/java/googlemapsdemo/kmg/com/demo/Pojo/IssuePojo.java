package googlemapsdemo.kmg.com.demo.Pojo;

public class IssuePojo {
    
    private String issueCategory;

    private String issueSubCat;

    public IssuePojo(String issueCategory, String issueSubCat) {
        this.issueCategory = issueCategory;
        this.issueSubCat = issueSubCat;
    }

    public String getTitle() {
        return issueCategory;
    }

    public void setTitle(String issueCategory) {
        this.issueCategory = issueCategory;
    }

    public String getSubtitle() {
        return issueSubCat;
    }

    public void setSubtitle(String issueSubCat) {
        this.issueSubCat = issueSubCat;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"issueCategory\": \"" + issueCategory + "\",\n" +
                "  \"issueSubCat\": \"" + issueSubCat + "\"\n" +
                "}";
    }
}
