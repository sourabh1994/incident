package googlemapsdemo.kmg.com.demo.Pojo;


public class SubCategoryPojo {
    private  String subtitle;
    private String title;

    public SubCategoryPojo(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"title\": \"" + title + "\",\n" +
                "  \"subtitle\": \"" + subtitle + "\"\n" +
                "}";
    }
}
