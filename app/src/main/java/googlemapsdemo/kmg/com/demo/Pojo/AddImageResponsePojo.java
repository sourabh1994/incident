package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddImageResponsePojo {
    @SerializedName("Message")
    @Expose
    private String Message;

    @SerializedName("Success")
    @Expose
    private String Success;

    @SerializedName("Data")
    @Expose
    private String Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"Message\": \"" + Message + "\",\n" +
                "  \"Success\": \"" + Success + "\",\n" +
                "  \"Data\": " + Data + "\n" +
                "}";
    }

}
