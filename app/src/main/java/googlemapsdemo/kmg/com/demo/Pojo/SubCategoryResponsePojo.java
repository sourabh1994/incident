package googlemapsdemo.kmg.com.demo.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoryResponsePojo {
    @SerializedName("Message")
    @Expose
    private String Message;

    @SerializedName("Success")
    @Expose
    private String Success;

    @SerializedName("Data")
    @Expose
    private List<SubCategoryResponseDataPojo> Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public List<SubCategoryResponseDataPojo> getData() {
        return Data;
    }

    public void setData(List<SubCategoryResponseDataPojo> data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"Message\": \"" + Message + "\",\n" +
                "  \"Success\": \"" + Success + "\",\n" +
                "  \"Data\": " + Data + "\n" +
                "}";
    }
}
